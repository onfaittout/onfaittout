# fichier contenant les fonctions qui font les requetes sql et retourne le resultat

def get_nb_visite_par_pays(cursor) -> list[dict]:
    return execute_query(cursor, """ SELECT pays.pays, SUM(voyages.duree) AS nb_jour_visite
        FROM pays
        JOIN voyages ON pays.id_pays = voyages.id_pays
        GROUP BY voyages.id_pays;
    """)


def get_all_personne_destination(cursor):
    return execute_query(cursor, """ SELECT 
	p.id_personne, 
	p.nom, 
	p.prenom, 
	pa2.pays AS pays_residence, 
	pa.pays, 
	v.duree 
FROM voyages AS v 
JOIN personnes AS p ON p.id_personne = v.id_personne 
JOIN pays AS pa ON pa.id_pays = v.id_pays 
JOIN pays AS pa2 ON p.pays_residence = pa2.id_pays;
    """)


def execute_query(cursor, query: str, **kwargs) -> list[dict]:
    print(query)
    cursor.execute(query, kwargs)
    result = cursor.fetchall()
    print(result)
    return result
