DROP DATABASE IF EXISTS onfaittout;
CREATE DATABASE onfaittout;
USE onfaittout;


CREATE TABLE pays (
  id_pays INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pays VARCHAR(100) NOT NULL,
  UNIQUE (pays)
);

CREATE TABLE personnes (
  id_personne INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  prenom VARCHAR(100) NOT NULL,
  nom VARCHAR(100) NOT NULL,
  pays_residence INT NOT NULL,
  FOREIGN KEY (pays_residence) REFERENCES pays (id_pays)
);

CREATE TABLE voyage (
  id_personne INT,
  id_pays INT,
  annee_depart INT,
  mois_depart INT,
  jour_depart INT,
  annee_arrive INT,
  mois_arrive INT,
  jour_arrive INT,
  FOREIGN KEY (id_personne) REFERENCES personnes (id_personne),
  FOREIGN KEY (id_pays) REFERENCES pays (id_pays)
);

-- table "pays"

insert into pays (id_pays, pays) values (1, 'Estonia');
insert into pays (id_pays, pays) values (2, 'Pakistan');
insert into pays (id_pays, pays) values (3, 'Philippines');
insert into pays (id_pays, pays) values (4, 'Lithuania');
insert into pays (id_pays, pays) values (5, 'Lebanon');
insert into pays (id_pays, pays) values (6, 'Poland');
insert into pays (id_pays, pays) values (7, 'China');
insert into pays (id_pays, pays) values (8, 'Austria');
insert into pays (id_pays, pays) values (9, 'United Kingdom');
insert into pays (id_pays, pays) values (10, 'France');


-- table "personnes"

insert into personnes (id_personne, prenom, nom, pays_residence) values (1, 'Laughton', 'Timmes', 2);
insert into personnes (id_personne, prenom, nom, pays_residence) values (2, 'Katine', 'Obern', 4);
insert into personnes (id_personne, prenom, nom, pays_residence) values (3, 'Bren', 'Linacre', 7);
insert into personnes (id_personne, prenom, nom, pays_residence) values (4, 'Marshal', 'Tilzey', 1);
insert into personnes (id_personne, prenom, nom, pays_residence) values (5, 'Ofella', 'Seamans', 3);
insert into personnes (id_personne, prenom, nom, pays_residence) values (6, 'Mirabella', 'Brookes', 7);
insert into personnes (id_personne, prenom, nom, pays_residence) values (7, 'Terese', 'Baurerich', 4);
insert into personnes (id_personne, prenom, nom, pays_residence) values (8, 'Johannah', 'Thaller', 7);
insert into personnes (id_personne, prenom, nom, pays_residence) values (9, 'Shina', 'Ulster', 9);
insert into personnes (id_personne, prenom, nom, pays_residence) values (10, 'Poppy', 'Bartolijn', 4);


-- table "voyage"


insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (10, 2, 1989, 3, 1, 1980, 6, 10);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (4, 3, 1969, 2, 7, 2018, 4, 3);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (3, 3, 1965, 11, 28, 2019, 2, 1);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (2, 7, 1966, 4, 27, 1985, 2, 7);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (3, 3, 1988, 12, 4, 2007, 1, 10);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (8, 1, 1973, 6, 30, 1987, 8, 11);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (5, 3, 1987, 1, 7, 1999, 3, 10);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (6, 5, 1982, 12, 31, 2017, 7, 5);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (5, 9, 1982, 8, 24, 2021, 9, 11);
insert into voyage (id_personne, id_pays, annee_depart, mois_depart, jour_depart, annee_arrive, mois_arrive, jour_arrive) values (4, 6, 1981, 9, 1, 2007, 1, 9);


alter table voyage 
drop column annee_depart, 
drop column mois_depart, 
drop column jour_depart, 
drop column annee_arrive, 
drop column mois_arrive;

alter table voyage change column jour_arrive duree int;



